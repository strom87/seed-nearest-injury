package main

import (
	"flag"
	"log"

	"bitbucket.org/strom87/db-nearest-injury"
	"bitbucket.org/strom87/seed-nearest-injury/seeder"
)

const (
	dbName       = "db-ni"
	dbConnection = "mongodb://localhost:27017"
)

func seed(conn *db.Connection) {
	seeder.NewUser().Seed(conn)
	seeder.NewSpot().Seed(conn)
	seeder.NewSpot().Index(conn)
	seeder.NewSpotComment().Seed(conn)
	seeder.NewSpotCommentReply().Seed(conn)
}

func remove(conn *db.Connection) {
	seeder.NewUser().Remove(conn)
	seeder.NewSpot().Remove(conn)
	seeder.NewSpotComment().Remove(conn)
	seeder.NewSpotCommentReply().Remove(conn)
}

func run(option string, conn *db.Connection) {
	switch option {
	case "seed":
		seed(conn)
	case "remove":
		remove(conn)
	case "reseed":
		remove(conn)
		seed(conn)
	default:
		log.Println("not a valid option:", option)
	}
}

func main() {
	option := flag.String("opt", "seed", "Flags: seed, reseed, remove")
	flag.Parse()

	conn := db.NewConnection(dbName, dbConnection)
	if err := conn.Open(); err != nil {
		log.Println("Could not open connection db connection.", err)
		return
	}
	defer conn.Close()

	run(*option, conn)
}
