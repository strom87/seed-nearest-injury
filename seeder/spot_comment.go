package seeder

import (
	"log"
	"time"

	"bitbucket.org/strom87/db-nearest-injury"
)

type SpotComment struct{}

func NewSpotComment() *SpotComment {
	return &SpotComment{}
}

func (SpotComment) Seed(conn *db.Connection) {
	data := []db.Comment{
		db.Comment{
			Id:         commentId[0],
			SpotId:     spotId[0],
			UserId:     users[1].Id,
			UserName:   users[1].Name,
			Message:    "Gött ska testas",
			HasReplies: true,
			Created:    time.Now(),
		},
		db.Comment{
			Id:         commentId[1],
			SpotId:     spotId[0],
			UserId:     users[2].Id,
			UserName:   users[2].Name,
			Message:    "Jaja, de verkar ju bra.",
			HasReplies: true,
			Created:    time.Now(),
		},
		db.Comment{
			Id:         commentId[2],
			SpotId:     spotId[2],
			UserId:     users[4].Id,
			UserName:   users[4].Name,
			Message:    "Blir att testa!",
			HasReplies: false,
			Created:    time.Now(),
		},
	}

	for _, comment := range data {
		if err := db.NewSpotCommentProvider(conn).Insert(comment); err != nil {
			log.Println("Comment insert error:", err)
		}
	}
}

func (SpotComment) Remove(conn *db.Connection) {
	db.NewSpotCommentProvider(conn).RemoveAll()
}
