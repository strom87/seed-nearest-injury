package seeder

import (
	"log"
	"time"

	"bitbucket.org/strom87/db-nearest-injury"
)

type User struct{}

func NewUser() *User {
	return &User{}
}

func (User) Seed(conn *db.Connection) {
	data := []db.User{
		db.User{
			Id:            users[0].Id,
			Name:          users[0].Name,
			Email:         "kalle@nearestinjury.com",
			IsActivated:   true,
			RegisterDate:  time.Now(),
			LastLoginDate: time.Now(),
			Credential:    getCredential(),
		},
		db.User{
			Id:            users[1].Id,
			Name:          users[1].Name,
			Email:         "felix@nearestinjury.com",
			IsActivated:   true,
			RegisterDate:  time.Now(),
			LastLoginDate: time.Now(),
			Credential:    getCredential(),
		},
		db.User{
			Id:            users[2].Id,
			Name:          users[2].Name,
			Email:         "janne@nearestinjury.com",
			IsActivated:   true,
			RegisterDate:  time.Now(),
			LastLoginDate: time.Now(),
			Credential:    getCredential(),
		},
		db.User{
			Id:            users[3].Id,
			Name:          users[3].Name,
			Email:         "hanna@nearestinjury.com",
			IsActivated:   true,
			RegisterDate:  time.Now(),
			LastLoginDate: time.Now(),
			Credential:    getCredential(),
		},
		db.User{
			Id:            users[4].Id,
			Name:          users[4].Name,
			Email:         "lotta@nearestinjury.com",
			IsActivated:   true,
			RegisterDate:  time.Now(),
			LastLoginDate: time.Now(),
			Credential:    getCredential(),
		},
		db.User{
			Id:            users[5].Id,
			Name:          users[5].Name,
			Email:         "a@a.com",
			IsActivated:   true,
			RegisterDate:  time.Now(),
			LastLoginDate: time.Now(),
			Credential:    getCredential(),
		},
	}

	for _, user := range data {
		if err := db.NewUserProvider(conn).Insert(user); err != nil {
			log.Println("User insert error:", err)
		}
	}
}

func (User) Remove(conn *db.Connection) {
	db.NewUserProvider(conn).RemoveAll()
}

// password = qwerty
func getCredential() db.Credential {
	return db.Credential{
		Salt:       "qA0tXLtzktJylyZBRafIfQ0P",
		Password:   "$2a$12$vIGwYHH6m2gg2OiK157wtuKeeezo5vV70YALwXba0RhwnUb9Lz8yi",
		RehashDate: time.Now(),
	}
}
