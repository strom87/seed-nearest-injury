package seeder

import (
	"log"
	"time"

	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type SpotCommentReply struct{}

func NewSpotCommentReply() *SpotCommentReply {
	return &SpotCommentReply{}
}

func (SpotCommentReply) Seed(conn *db.Connection) {
	data := []db.CommentReply{
		db.CommentReply{
			Id:        bson.NewObjectId(),
			CommentId: commentId[0],
			UserId:    users[2].Id,
			UserName:  users[2].Name,
			Message:   "Håller med du!",
			Created:   time.Now(),
		},
		db.CommentReply{
			Id:        bson.NewObjectId(),
			CommentId: commentId[0],
			UserId:    users[3].Id,
			UserName:  users[3].Name,
			Message:   "Ja du, kanske de är så då.",
			Created:   time.Now(),
		},
		db.CommentReply{
			Id:        bson.NewObjectId(),
			CommentId: commentId[0],
			UserId:    users[0].Id,
			UserName:  users[0].Name,
			Message:   "Ja så e de.",
			Created:   time.Now(),
		},
		db.CommentReply{
			Id:        bson.NewObjectId(),
			CommentId: commentId[1],
			UserId:    users[0].Id,
			UserName:  users[0].Name,
			Message:   "Håller med du!",
			Created:   time.Now(),
		},
		db.CommentReply{
			Id:        bson.NewObjectId(),
			CommentId: commentId[1],
			UserId:    users[4].Id,
			UserName:  users[4].Name,
			Message:   "Gött",
			Created:   time.Now(),
		},
	}

	for _, reply := range data {
		if err := db.NewSpotCommentReplyProvider(conn).Insert(reply); err != nil {
			log.Println("Comment reply insert error:", err)
		}
	}
}

func (SpotCommentReply) Remove(conn *db.Connection) {
	db.NewSpotCommentReplyProvider(conn).RemoveAll()
}
