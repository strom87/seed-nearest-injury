package seeder

import "gopkg.in/mgo.v2/bson"

type userTemplate struct {
	Id   bson.ObjectId
	Name string
}

var users = []userTemplate{
	userTemplate{
		Id:   bson.ObjectIdHex("55f146b659f76f721943a000"),
		Name: "Kalle",
	},
	userTemplate{
		Id:   bson.ObjectIdHex("55f146b659f76f721943a001"),
		Name: "Felix",
	},
	userTemplate{
		Id:   bson.ObjectIdHex("55f146b659f76f721943a002"),
		Name: "Janne",
	},
	userTemplate{
		Id:   bson.ObjectIdHex("55f146b659f76f721943a003"),
		Name: "Hanna",
	},
	userTemplate{
		Id:   bson.ObjectIdHex("55f146b659f76f721943a004"),
		Name: "Lotta",
	},
	userTemplate{
		Id:   bson.ObjectIdHex("55f146b659f76f721943a005"),
		Name: "Fia",
	},
}

var spotId = []bson.ObjectId{
	bson.ObjectIdHex("55f146b659f76f721943b000"),
	bson.ObjectIdHex("55f146b659f76f721943b001"),
	bson.ObjectIdHex("55f146b659f76f721943b002"),
}

var commentId = []bson.ObjectId{
	bson.ObjectIdHex("55f146b659f76f721943c000"),
	bson.ObjectIdHex("55f146b659f76f721943c001"),
	bson.ObjectIdHex("55f146b659f76f721943c002"),
}
