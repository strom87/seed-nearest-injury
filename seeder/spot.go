package seeder

import (
	"log"
	"time"

	"bitbucket.org/strom87/db-nearest-injury"
	"gopkg.in/mgo.v2/bson"
)

type Spot struct{}

func NewSpot() *Spot {
	return &Spot{}
}

func (s Spot) Seed(conn *db.Connection) {
	data := []db.Spot{
		db.Spot{
			Id:          spotId[0],
			UserId:      users[0].Id,
			UserName:    users[0].Name,
			Name:        "Oskar Fredriks kyrka",
			Description: "En kyrka man kan åka till",
			Tags:        []string{"skate", "longboard"},
			Location:    s.getLocation(57.696591, 11.947505),
			Created:     time.Now(),
			Rating: db.Rating{
				Score: 4,
				Values: map[string]int64{
					"1": 0,
					"2": 0,
					"3": 1,
					"4": 0,
					"5": 1,
				},
				RatedBy: []bson.ObjectId{
					users[1].Id,
					users[2].Id,
				},
			},
		},
		db.Spot{
			Id:          spotId[1],
			UserId:      users[1].Id,
			UserName:    users[1].Name,
			Name:        "Ullevi",
			Description: "Bra ställe att åka och spela boll på",
			Tags:        []string{"skate", "bmx"},
			Location:    s.getLocation(57.7059133, 11.947505),
			Created:     time.Now(),
			Rating: db.Rating{
				Score: 2.3,
				Values: map[string]int64{
					"1": 0,
					"2": 2,
					"3": 1,
					"4": 0,
					"5": 0,
				},
				RatedBy: []bson.ObjectId{
					users[3].Id,
					users[4].Id,
					users[2].Id,
				},
			},
		},
		db.Spot{
			Id:          spotId[2],
			UserId:      users[5].Id,
			UserName:    users[5].Name,
			Name:        "Askimsbadet",
			Description: "Har en brygga man kan åka på",
			Tags:        []string{"bmx", "downhill"},
			Location:    s.getLocation(57.625411, 11.926552),
			Created:     time.Now(),
			Rating: db.Rating{
				Score: 4,
				Values: map[string]int64{
					"1": 0,
					"2": 0,
					"3": 1,
					"4": 2,
					"5": 1,
				},
				RatedBy: []bson.ObjectId{
					users[0].Id,
					users[1].Id,
					users[2].Id,
					users[4].Id,
				},
			},
		},
	}

	for _, spot := range data {
		if err := db.NewSpotProvider(conn).Insert(spot); err != nil {
			log.Println("Spot insert error:", err)
		}
	}
}

func (Spot) Index(conn *db.Connection) {
	if err := db.NewSpotProvider(conn).IndexLocationGeo(); err != nil {
		log.Println("Spot index error:", err)
	}
}

func (Spot) Remove(conn *db.Connection) {
	db.NewSpotProvider(conn).RemoveAll()
}

func (Spot) getLocation(lng, lat float64) db.Location {
	return db.Location{
		Type:        "Point",
		Coordinates: []float64{lng, lat},
	}
}
